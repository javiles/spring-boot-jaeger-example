package com.sixbell.jaeger.example.sum.controller;

import com.sixbell.jaeger.example.sum.service.SumService;
import io.jaegertracing.internal.JaegerSpanContext;
import io.opentracing.Span;
import io.opentracing.Tracer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class SumController {

    private static final Logger LOG = LoggerFactory.getLogger(SumController.class);

    @Autowired
    private Tracer tracer;

    private SumService sumService;

    @Autowired
    public void setSumService(SumService sumService) {
        this.sumService = sumService;
    }

    @GetMapping("/sum0")
    public int sum0(@RequestParam(required = true) int a, @RequestParam(required = true) int b) {
        int result = this.sumService.sum0(a,b);
        return result;
    }

    @GetMapping("/sum1")
    public int sum1(@RequestParam(required = true) int a, @RequestParam(required = true) int b) {
        Span span = tracer.buildSpan("SumController: sum endpoint").start();
        int result = this.sumService.sum1(a,b);
        span.finish();
        return result;

    }

    @GetMapping("/sum2")
    public int sum2(@RequestParam(required = true) int a, @RequestParam(required = true) int b) {
        Span span = tracer.buildSpan("SumController: sum endpoint").asChildOf(tracer.activeSpan()).start();
        tracer.activateSpan(span);
        int result = this.sumService.sum2(a,b);
        span.finish();
        return result;

    }

    @GetMapping("/sum3")
    public int sum3(@RequestParam(required = true) int a, @RequestParam(required = true) int b) {
        int result = this.sumService.sum3(a,b);
        return result;
    }

    @GetMapping("/slowService")
    public String slowService() {
        String result = this.sumService.slowService();
        return result;
    }

}
