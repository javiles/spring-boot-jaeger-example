package com.sixbell.jaeger.example.sum.service;

import io.jaegertracing.internal.JaegerSpanContext;
import io.opentracing.Span;
import io.opentracing.Tracer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class SumService {
    @Autowired
    private Tracer tracer;

    private static final Logger LOG = LoggerFactory.getLogger(SumService.class);


    public int sum0(int a, int b){
        int sum = a + b;
        return sum;
    }

    public int sum1(int a, int b){
        Span span = tracer.buildSpan("SumService: sum method").start();
        int sum = a + b;
        span.finish();
        return sum;
    }

    public int sum2(int a, int b){
        Span span = tracer.buildSpan("SumService: sum method").asChildOf(tracer.activeSpan()).start();
        int sum = a + b;
        span.finish();
        return sum;
    }

    public int sum3(int a, int b){
        Span spanA = tracer.activeSpan();
        for (Map.Entry<String,String> entry : spanA.context().baggageItems())
            LOG.info("Key = " + entry.getKey() + ", Value = " + entry.getValue());
        Span span = tracer.buildSpan("SumService: sum method").asChildOf(spanA).start();
        int sum = a + b;
        span.log("SUM OK : " + sum);
        span.finish();
        return sum;
    }

   public String slowService(){
       Span spanA = tracer.activeSpan();
       for (Map.Entry<String,String> entry : spanA.context().baggageItems())
           LOG.info("Key = " + entry.getKey() + ", Value = " + entry.getValue());
       Span span = tracer.buildSpan("SumService: slow method").asChildOf(spanA).start();

       try {
           Thread.sleep(5000);
       } catch (InterruptedException e) {
           e.printStackTrace();
       }

       span.log("Slow Service : finish" );
       span.finish();

       return "OK";
   }


}












