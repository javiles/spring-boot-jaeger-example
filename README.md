# Build

```
docker-compose build
```
# Run
```
docker-compose up
```
# Info Links

## OpenTracing Specification
https://opentracing.io/specification/
## Jaeger Architecture
https://www.jaegertracing.io/docs/1.21/architecture/
## SPAN Definition: IMPORTANT
https://opentracing.io/docs/overview/spans/
## Jaeger Presentation Intro
https://www.youtube.com/watch?v=cXoTja7BvSA
## Jaeger Presentation 
https://www.youtube.com/watch?v=zb0fdU6c0KU
## java-spring-jaeger: documentation of library used in this project
https://github.com/opentracing-contrib/java-spring-jaeger
## Other Opentracing Implementation Libraries
https://github.com/opentracing-contrib?q=java-spring&type=&language=
