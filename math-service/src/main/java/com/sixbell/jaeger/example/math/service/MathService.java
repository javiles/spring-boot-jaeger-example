package com.sixbell.jaeger.example.math.service;


import io.jaegertracing.internal.JaegerSpanContext;
import io.opentracing.Span;
import io.opentracing.Tracer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

@Service
public class MathService {

    private static final Logger LOG = LoggerFactory.getLogger(MathService.class);

    @Autowired
    private Tracer tracer;

    private RestTemplate restTemplate;
    private final String sumServiceHost;
    private final String sumServicePort;

    public MathService(@Value("${service.sumHost}") String sumServiceHost,
                       @Value("${service.sumPort}") String sumServicePort,
                       @Qualifier("sumRestTemplate") RestTemplate restTemplate) {
        this.sumServiceHost = sumServiceHost;
        this.sumServicePort = sumServicePort;
        this.restTemplate = restTemplate;
    }


    public int math0(int a, int b, int c){
        int result;
        String url = "http://" + this.sumServiceHost + ":" + this.sumServicePort +"/sum0?a="+a+"&b="+b;
        result = restTemplate.getForObject(url, Integer.class);
        result = result * c;
        return result;
    }
    public int math1(int a, int b, int c){
        Span span = tracer.buildSpan("MathService: math method").start();
        int result;
        String url = "http://" + this.sumServiceHost + ":" + this.sumServicePort +"/sum1?a="+a+"&b="+b;
        result = restTemplate.getForObject(url, Integer.class);
        result = result * c;
        span.finish();
        return result;
    }
    public int math2(int a, int b, int c){
        Span span = tracer.buildSpan("MathService: math method").asChildOf(tracer.activeSpan()).start();
        tracer.activateSpan(span);
        int result;
        String url = "http://" + this.sumServiceHost + ":" + this.sumServicePort +"/sum2?a="+a+"&b="+b;
        result = restTemplate.getForObject(url, Integer.class);
        result = result * c;
        span.finish();
        return result;
    }
    public int math3(int a, int b, int c){
        String callId = UUID.randomUUID().toString();
        Span span = tracer.buildSpan("MathService: math endpoint")
                .asChildOf(tracer.activeSpan())
                .withTag("uuid", callId)
                .withTag("special_tag","sixbell")
                .start();
        span.log("Elvis was here!");
        span.setBaggageItem("MessageToOthersServices", "Hello from service math");
        span.setBaggageItem("callId", callId);
        tracer.activateSpan(span);

        int result;
        String url = "http://" + this.sumServiceHost + ":" + this.sumServicePort +"/sum3?a="+a+"&b="+b;
        result = restTemplate.getForObject(url, Integer.class);
        result = result * c;
        span.finish();
        return result;
    }
    public int math4(int a, int b, int c){
        String callId = UUID.randomUUID().toString();
        Span parentSpan = tracer.activeSpan();

        Span span = tracer.buildSpan("MathService: call sum endpoint")
                .asChildOf(parentSpan)
                .withTag("uuid", callId)
                .withTag("special_tag","sixbell")
                .start();
        span.log("Elvis was here!");
        span.setBaggageItem("MessageToOthersServices", "Hello from service math");
        span.setBaggageItem("callId", callId);
        tracer.activateSpan(span);

        int result;
        String url = "http://" + this.sumServiceHost + ":" + this.sumServicePort +"/sum3?a="+a+"&b="+b;
        result = restTemplate.getForObject(url, Integer.class);
        result = result * c;

        Span spanB = tracer.buildSpan("MathService: call slow endpoint")
                .asChildOf(span)
                .withTag("uuid", callId)
                .withTag("special_tag","sixbell")
                .start();
        spanB.log("Elvis was here!");
        spanB.setBaggageItem("MessageToOthersServices", "Hello from service math");
        spanB.setBaggageItem("callId", callId);
        tracer.activateSpan(spanB);

        String urlB = "http://" + this.sumServiceHost + ":" + this.sumServicePort +"/slowService";
        String slowResult = restTemplate.getForObject(urlB, String.class);

        spanB.finish();
        span.finish();

        return result;
    }
    public int math5(int a, int b, int c){
        String callId = UUID.randomUUID().toString();
        Span parentSpan = tracer.activeSpan();

        Span span = tracer.buildSpan("MathService: call sum endpoint")
                .asChildOf(parentSpan)
                .withTag("uuid", callId)
                .withTag("special_tag","sixbell")
                .start();
        span.log("Elvis was here!");
        span.setBaggageItem("MessageToOthersServices", "Hello from service math");
        span.setBaggageItem("callId", callId);
        tracer.activateSpan(span);

        int result;
        String url = "http://" + this.sumServiceHost + ":" + this.sumServicePort +"/sum3?a="+a+"&b="+b;
        result = restTemplate.getForObject(url, Integer.class);
        result = result * c;
        span.finish();

        Span spanB = tracer.buildSpan("MathService: call slow endpoint")
                .asChildOf(parentSpan)
                .withTag("uuid", callId)
                .withTag("special_tag","sixbell")
                .start();
        spanB.log("Elvis was here!");
        spanB.setBaggageItem("MessageToOthersServices", "Hello from service math");
        spanB.setBaggageItem("callId", callId);
        tracer.activateSpan(spanB);

        String urlB = "http://" + this.sumServiceHost + ":" + this.sumServicePort +"/slowService";
        String slowResult = restTemplate.getForObject(urlB, String.class);

        spanB.finish();


        return result;
    }


}
