package com.sixbell.jaeger.example.math.controller;

import com.sixbell.jaeger.example.math.service.MathService;
import io.jaegertracing.internal.JaegerSpanContext;
import io.opentracing.Span;
import io.opentracing.Tracer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping
public class MathController {

    private static final Logger LOG = LoggerFactory.getLogger(MathController.class);

    @Autowired
    private Tracer tracer;

    private MathService mathService;

    @Autowired
    public void setMathService(MathService mathService) {
        this.mathService = mathService;
    }

    @GetMapping("/math0")
    public int math0(@RequestParam(required = true) int a, @RequestParam(required = true) int b, @RequestParam(required = true) int c){
        int result = this.mathService.math0(a,b,c);
        return result;
    }

    @GetMapping("/math1")
    public int math1(@RequestParam(required = true) int a, @RequestParam(required = true) int b, @RequestParam(required = true) int c){
        Span span = tracer.buildSpan("MathController: math endpoint").start();
        int result = this.mathService.math1(a,b,c);
        span.finish();
        return result;
    }

    @GetMapping("/math2")
    public int math2(@RequestParam(required = true) int a, @RequestParam(required = true) int b, @RequestParam(required = true) int c){
        Span span = tracer.buildSpan("MathController: math endpoint").asChildOf(tracer.activeSpan()).start();
        tracer.activateSpan(span);
        int result = this.mathService.math2(a,b,c);
        span.finish();
        return result;
    }

    @GetMapping("/math3")
    public int math3(@RequestParam(required = true) int a, @RequestParam(required = true) int b, @RequestParam(required = true) int c){
        int result = this.mathService.math3(a,b,c);
        return result;
    }


    @GetMapping("/math4")
    public int math4(@RequestParam(required = true) int a, @RequestParam(required = true) int b, @RequestParam(required = true) int c){
        int result = this.mathService.math4(a,b,c);
        return result;
    }

    @GetMapping("/math5")
    public int math5(@RequestParam(required = true) int a, @RequestParam(required = true) int b, @RequestParam(required = true) int c){
        int result = this.mathService.math5(a,b,c);
        return result;
    }

}
